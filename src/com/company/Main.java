package com.company;

import java.util.Random;

public class Main {
    private static final int n = 20;
    static final int m = 500;

    public static void main(String[] args) {
        Random random = new Random();

        int[] arr = new int[n];
        for(int i=0; i<n; i++){
            arr[i] = random.nextInt(m);
        }

        System.out.println("arr:");
        for(Integer el : arr){
            System.out.print(el+" ");
        }

        Integer[] res = ForkJoinQuickShellSort.startForkJoinQuickShellSort(arr);

        System.out.println("\nRESULT:");
        for(Integer el : res){
            System.out.print(el+" ");
        }
    }
}
