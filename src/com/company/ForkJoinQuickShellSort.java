package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ForkJoinQuickShellSort  extends RecursiveTask<Integer[]> {

    private final Integer[] array;

    private ForkJoinQuickShellSort(Integer[] array){
        this.array = array;
    }

    @Override
    protected Integer[] compute() {
        int threshold = 8;
        if(array.length <= threshold){
            shellSort();
            return array;
        }

        ArrayList<Integer> listSmall = new ArrayList<>();
        ArrayList<Integer> listEq = new ArrayList<>();
        ArrayList<Integer> listBig = new ArrayList<>();

        for (Integer integer : array) {
            if (integer <= (Main.m/3)) {
                listSmall.add(integer);
            }
            if ( (integer>(Main.m/3))&(integer<(Main.m/3*2)) ) {
                listEq.add(integer);
            }
            if (integer >= (Main.m/3*2)) {
                listBig.add(integer);
            }
        }

        ForkJoinQuickShellSort firstTask = new ForkJoinQuickShellSort(listSmall.toArray(new Integer[0]));
        firstTask.fork();

        ForkJoinQuickShellSort secondTask = new ForkJoinQuickShellSort(listEq.toArray(new Integer[0]));

        ForkJoinQuickShellSort thirdTask = new ForkJoinQuickShellSort(listBig.toArray(new Integer[0]));

        Integer[] thirdTaskResult = thirdTask.compute();
        Integer[] secondTaskResult = secondTask.compute();
        Integer[] firstTaskResult = firstTask.join();

        return concatenate(firstTaskResult,secondTaskResult,thirdTaskResult);
    }

    static Integer[] startForkJoinQuickShellSort(int[] array) {
        Integer[] arr = Arrays.stream( array ).boxed().toArray( Integer[]::new );
        ForkJoinTask<Integer[]> task = new ForkJoinQuickShellSort(arr);
        return new ForkJoinPool().invoke(task);
    }

    private void shellSort() {

        int h = 1;
        while (h*3 < array.length)
            h = h * 3 + 1;

        while(h >= 1) {
            hSort(h);
            h = h/3;
        }
    }
    private void hSort(int h) {
        int length = array.length;
        for (int i = h; i < length; i++) {
            for (int j = i; j >= h; j = j - h) {
                if (array[j] < array[j - h])
                    swap(j, j - h);
                else
                    break;
            }
        }
    }
    private void swap(int i, int j) {
        Integer temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    private Integer[] concatenate(Integer[] a, Integer[] b, Integer[] c) {
        int aLen = a.length;
        int bLen = b.length;
        int cLen = c.length;

        Integer[] res = (Integer[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen + cLen);
        System.arraycopy(a, 0, res, 0, aLen);
        System.arraycopy(b, 0, res, aLen, bLen);
        System.arraycopy(c, 0, res, aLen+bLen, cLen);

        return res;
    }

}
